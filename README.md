The system is checked on Ubuntu 16.04 with 

* python version 2.7.12
* django version 1.10.5

### Install python

```
sudo apt-get update
```

```
sudo apt-get install python
```

### Install pip
```
sudo apt-get install python-pip
```

### Install Django

```
pip install django==1.10
```

### Install and Configure MySql
```
sudo apt-get update
```

```
sudo apt-get install mysql-server
```

To configure MySQL:

```
sudo mysql_secure_installation
```

```
sudo mysql_install_db
```

### Start and Connect to MySQL

```
sudo service mysql start
```

If you have set a username and password, you can connect to localhost with the following command:

```
mysql -u DBUSER -p
```

### Install libraries for NLP

1. Install NLTK: 
```
sudo pip install -U nltk
```

2. Install Numpy (optional): 
```
sudo pip install -U numpy
```
    
3. Test installation
```
python 
```
```
import nltk
```

4. Download and install nltk data

First create nltk data folder and give read, write and execute access to it:
```
sudo mkdir /usr/share/nltk_data
```

```
sudo chmod 777 /usr/share/nltk_data
```

Then set the path to NTLK_DATA env:
```
export NLTK_DATA=/usr/share/nltk_data
```

Note: also put this line to .bashrc file

Download some nltk libraries:
```
python 
```

```
import nltk
```

```
nltk.download('wordnet')
```

```
nltk.download('omw')
```

```
nltk.download('punkt')
```

```
nltk.download('averaged_perceptron_tagger')
```

### Install more libraries for the project

1.
```
sudo pip install social-auth-app-django
```

2.
```
sudo apt-get install python-mysqldb
```

or

```
pip install mysql-python
```

3.
```
sudo pip install python-social-auth
```

4.
```
sudo pip install djangorestframework
```

### Next Steps...
* Go to MySQL prompt and create a database with name ***radfaDB***:

    ```
    CREATE DATABASE radfaDB;
    ```

* Then navigate to the ***settings.py*** file of the project and set the name, username and password for MySQL connection. For example:
    * 'NAME': 'radfaDB',
    * 'USER': 'root',
    * 'PASSWORD': 'root',

 
* Then synchronize the project with the database:
    Go to the directory of the project and execute the following:

    ```
    python manage.py makemigrations
    ```

    Then

    ```
    python manage.py migrate
    ```

    or

    ```
    python manage.py migrate --fake
    ```

    Finally:

    ```
    python manage.py migrate --run-syncdb
    ```

* Set UTF-* to the databases:
    Go to mysql prompt and execute:

    ```
    ALTER DATABASE radfaDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
    ```

    ```
    USE radfaDB;
    ```

    ```
    ALTER TABLE RadFa_py_facet CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
    ```

    ```
    ALTER TABLE RadFa_py_history CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
    ```
    
* Run the project :
    
    Go to the directory of the project and execute:

    ```
    python manage.py runserver  
    ```
    
* Go to a browser and type the following url:

    http://localhost:8000/login/
