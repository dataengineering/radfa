from post import Post

"""
Represents a Comment , filtered from the search procedure.
<Comment> is a subclass of <Post> class, thus it inherits its fields.
It also contains some extra fields that represent the origin post, i.e. the post where this <Comment> commented at.
Extra Fields:
        - original_created_time: the updated time in human readable format of the origin post
        - original_message: the message (text) of the origin post
        - original_uid: the id of the user who created the origin post
        - original_uname: the name of the user who created the origin post
Format in Json:
        { "pid": post_id, "uname": uname, "uid": uid, "updated_time": time, "created_time": time, 
          "message": msg, "comments": 0, "likes": 0, "is_comment": True, "original_created_time": time,
          "original_message": msg, "original_uname": uname, "original_uid": uid }
"""
class Comment(Post):
    
    def __init__(self):
        Post.__init__(self, is_comment = True)
        self.original_created_time = ""
        self.original_message = ""
        self.original_uid = ""
        self.original_uname = ""


    """
    Get the comment and the origin post (in which post the comment was made) and set the appropriate values in the instance.
    It calls the <setPost()> method from the <Post> parent class

    Arguments:
        - jsonComment: the comment in json format
        - jsonPost: the post in json format
    """
    def setComment(self, jsonComment, jsonPost):
        try:
            self.setPost(jsonComment)

            if 'message' in jsonPost:
                self.original_message = jsonPost['message']

            if 'created_time' in jsonPost:
                self.original_created_time = jsonPost['created_time']

            if 'id' in jsonPost:
                self.original_uid = jsonPost['id']

            if 'name' in jsonPost:
                self.original_uname = jsonPost['name']
        except Exception:
            print 'Comment Class :: Error in setting a comment'
            raise
        
            
