import traceback, sys

"""
Represents a set of information that contains the discriminative Statistics of a User.
Fields:
        - main_key: the value of an attribute (keyword or phrase) of a facet as it is given by the RadFa user 
        - synonyms: a list of synonyms of the main key
        - cardinality: the total number that the main key (and its synonyms) appear in this User's messages
Format in JSON:
        { "main_key": value, "synonyms": [ "syn1", ... ] , "cardinality": #appearance}
"""
class Note:

    def __init__(self, main_key_="", synonym_="", cardinality_=0):
        self.main_key = ""
        self.synonyms = []
        self.cardinality = 0

        if not main_key_ == "":
            self.main_key = main_key_

        if not synonym_ == "":
            self.synonyms.append(synonym_)

        if not cardinality_ == 0:
            self.cardinality = cardinality_

    def addSynonym(self, synonym, count):
        try:
            if isinstance(synonym, list):
                for syn in synonym:
                    if not syn in self.synonyms:
                        self.synonyms.append(syn)
            else:
                if ((not synonym in self.synonyms) or (not synonym == "")):
                    self.synonyms.append(synonym)

            self.cardinality = self.cardinality + count
        except Exception as e:
            print "Note Class :: Error in adding synonyms in an existing Note  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise