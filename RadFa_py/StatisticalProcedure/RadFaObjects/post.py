import traceback, sys

"""
Represents a Post, filtered from the search procedure.
Fields:
        - pid: the id of the Post
        - updated_time: the timestamp where the post was been created
        - created_time: the updated time in human readable format
        - message: the core of the post (its text)
        - uid: the id of the user who created the post
        - uname: the name of the user who creted the post
        - comments: the number of the comments that got this post
        - likes: the number of the likes that got this post
        - is_comment: if it is an original post or a comment
Format in Json:
        { "pid": post_id, "uname": uname, "uid": uid, "updated_time": time, "created_time": time, 
          "message": msg, "comments": #com, "likes": #likes, "is_comment": False }
"""
class Post:

    def __init__(self, is_comment=False):
        self.pid = 0
        self.updated_time = ""
        self.created_time = ""
        self.message = ""
        self.uid = 0
        self.uname = ""
        self.comments = 0
        self.likes = 0
        self.is_comment = is_comment


    def setPost(self, jsonPost):
        try:
            if 'message' in jsonPost:
                self.message = jsonPost['message']

            if 'id_p' in jsonPost:
                self.pid = jsonPost['id_p']

            if 'updated_time' in jsonPost:
                self.updated_time = jsonPost['updated_time']

            if 'created_time' in jsonPost:
                self.created_time = jsonPost['created_time']

            if 'id' in jsonPost:
                self.uid = jsonPost['id']

            if 'name' in jsonPost:
                self.uname = jsonPost['name']
            
            if self.is_comment:   
                if 'from' in jsonPost:
                    self.uid = jsonPost['from']['id']
                    self.uname = jsonPost['from']['name']
            else:
                if 'comments' in jsonPost:
                    self.comments = len(jsonPost['comments'])
                    
                if 'likes' in jsonPost:
                    self.likes = len(jsonPost['likes'])
        except Exception as e:
            print "Post Class :: Error in setting a post  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
        
        
                             