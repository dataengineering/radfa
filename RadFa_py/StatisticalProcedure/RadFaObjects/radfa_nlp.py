# -*- coding: utf-8 -*-
#!/usr/bin/python

import nltk
import re
import sys, traceback
reload(sys)
sys.setdefaultencoding('utf-8')

from nltk.corpus import wordnet as wn
from nltk.stem.snowball import SnowballStemmer
from GreekStemmer import Stemmer


"""
This Class contains RadFa's nlp functionality, such as: 
find synonyms of a word, apply stemming in a word, determines the times a word or its synonyms appear in a text etc.
Fields:
        - language: the applied language (Greek and English)
        - stemmer: the applied stemmer regarding the language
"""
class RadfaNLP:
    def __init__(self):
        self.language = None
        self.stemmer = None


    """
    Set the language and the Stemmer of the search by checking the first value of the keywords/phrases list.
    For the moment Greek and English languages are available.
        - Parameters:
                     keys: the list of the keywords/phrases
        - Return Value: void
    """
    def setFields(self, keys):
        try:
            #match by unicode -> if True then language is Greek
            if isinstance(keys[0][0], list):
                match = re.match(ur"^[\u0370-\u03FF]*$", keys[0][0][0]) #U+0370–U+03FF
            else:
                match = re.match(ur"^[\u0370-\u03FF]*$", keys[0][0]) #U+0370–U+03FF

            if match is not None:
                self.language = 'ell'
                self.stemmer = Stemmer()
            else:
                self.language = 'eng'
                self.stemmer = SnowballStemmer("english")
        except Exception:
            print "RadfaNLP Class :: Error in setting fields. Please check the language used in the facet %s" % e
            traceback.print_exc(file=sys.stdout)
            raise


    """
    Checkes whether a synonym exists in a list of words.
        - Parameters: 
                     synonym_set -> a list of synonyms
                     token_list -> a list of tuples where its tuple contains a word and its POS
        - Return Value: True/False
    """
    def exist_synonym(self, synonym_set, token_list):
        #print 'lang = ',self.language

        try:
            for tok_tuple in token_list:
                for syn in synonym_set:
                    if self.language == 'eng':
                        word = self.stemmer.stem(tok_tuple[0])
                    elif self.language == 'ell':
                        try:
                            word = self.stemmer.stem_word(tok_tuple[0], tok_tuple[1])
                        except ValueError:
                            continue
                    if ((syn == word) or (len(word)>=len(syn) and word.startswith(syn)) ):
                        return True

        except Exception as e:
            print "RadfaNLP Class :: Error in finding synonyms. Please check the Stemmer and the Language %s" % e
            traceback.print_exc(file=sys.stdout)
            raise


    """
    Find the synonyms of the given word.
        - Parameters:
                     word -> the given word
                     with_stem -> defines whether to find the stemming part of each synonym or not
        - Return Value: a list of synonyms or a list of stemmed synonyms
    """
    def get_synonyms(self, word, with_stem=True):
        temp_keyset = [word]

        try:
            #for each synonym get its sense, i.e. how close is the meaning of the synonym
            for syn in wn.synsets(word, lang=self.language):
                
                try:
                    sense = int(syn._name.split('.')[2])
                except ValueError:
                    sense = 1

                # keep only the lemmas with a number sense below 02 (very close synonyms) and get their stemming.
                if sense <= 2:
                    for l in syn.lemma_names(lang=self.language):
                        if with_stem:
                            if self.language == 'eng':
                                l = self.stemmer.stem(l)
                            elif self.language == 'ell':
                                l = self.stemmer.stem_word(l, syn._pos)

                        if l not in temp_keyset:
                            temp_keyset.append(l)

        except Exception as e:
            print "RadfaNLP Class :: Error in getting synonyms. Please check the Stemmer and the Language %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
        finally:
            return temp_keyset


    """
    Find the times that a word appears in a list of tokens.
        - Parameters:
                     syn -> the given word
                     token_list-> a list of tuples where its tuple contains a word and its POS
        - Return Value: the cardinality
    """
    def count_synonyms(self, syn, token_list):
        count = 0
        try:
            for tok_tuple in token_list:
                if self.language == 'eng':
                    word = self.stemmer.stem(tok_tuple[0])
                elif self.language == 'ell':
                    try:
                        word = self.stemmer.stem_word(tok_tuple[0], tok_tuple[1])
                    except ValueError:
                        continue

                if ((syn == word) or (len(word)>=len(syn) and word.startswith(syn)) ):
                    count = count + 1
        except Exception as e:
            print Exception, "RadfaNLP Class :: Error in counting synonyms. Please check the Stemmer and the Language %s" % e, sys.exc_info()[2]
            traceback.print_exc(file=sys.stdout)
            raise
        finally:
            return count