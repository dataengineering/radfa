from user import User
import traceback, sys

"""
Represents the Resulted Data after the filtering application in the Statistical and Searching Procedure.
The resulted data are grouped by <User>.
Fields:
        - users: list of the resulted Users
Format in JSON:
        { "users": [ User1, User2 , ... , UserN ]}
"""
class ResultedData:

    def __init__(self):
        self.users = []

    def getUser(self, uid):
        exist_user = [user for user in self.users if user.uid == uid]

        try:
            return exist_user[0]
        except IndexError:
            return None

    def createUser(self, uid, uname):
        try:
            new_user = User(uid, uname)
            self.users.append(new_user)
        except Exception as e:
            print "ResultedData Class :: Error in creating a new user  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def addPost(self, uid, post):
        try:
            user = self.getUser(uid)
            user.addPost(post)
        except Exception as e:
            print "ResultedData Class :: Error in adding a new post to the user  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def addComment(self, uid, comment, origin_post):
        try:
            user = self.getUser(uid)
            user.addComment(comment, origin_post)
        except Exception as e:
            print "ResultedData Class :: Error in adding a new comment to the user %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def addNote(self, uid, main_key, cardinality=1):
        try:
            user = self.getUser(uid)
            user.addNote(main_key, "", cardinality)
        except Exception as e:
            print "ResultedData Class :: Error in adding a new note to the user %s" % e
            traceback.print_exc(file=sys.stdout)
            raise