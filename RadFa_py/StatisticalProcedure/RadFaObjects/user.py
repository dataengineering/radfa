from post import Post
from comment import Comment
from note import Note
import traceback, sys

"""
Represents the resulted FB.User which contains all the suitable information about the filtered resulted data 
derived and determined from the Statistical and Searching Procedure.
Fields:
        - uid: the id of the user
        - name: the name of the user
        - posts: list of the user's filtered posts
        - comments: list of the user's filtered comments
        - notes: user's notes
Format in JSON:
        { "uid": id , "name": name, "posts": [Post1, Post2, ...], "comments": [Comment1, ... ], "notes": [Notes1, ...] }
"""
class User:

    def __init__(self, uid, name):
        self.uid = uid
        self.name = name
        self.posts = []
        self.comments = []
        self.notes = []
        
        
    def addPost(self, post):
        try:
            alreadyIn = [p for p in self.posts if p.pid == post['id_p']]

            if len(alreadyIn) == 0:
                new_post = Post()
                new_post.setPost(post)
                self.posts.append(new_post)
        except Exception as e:
            print "User Class :: Error in adding a post  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def addComment(self, comment, origin_post):
        try:
            alreadyIn = [c for c in self.comments if c.pid == comment['id']]
            
            if len(alreadyIn) == 0:
                new_comment = Comment()
                new_comment.setComment(comment, origin_post)
                self.comments.append(new_comment)
        except Exception as e:
            print "User Class :: Error in adding a comment  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def addNote(self, main_key, synonym="", count=1):
        try:
            exist_note = [n for n in self.notes if n.main_key == main_key]
            
            if len(exist_note) == 0:
                new_note = Note(main_key_=main_key, synonym_=synonym, cardinality_=count)
                self.notes.append(new_note)
            else:
                exist_note[0].addSynonym(synonym, count)

        except Exception as e:
            print "User Class :: Error in adding a note  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    def copyNote(self, note_):
        try:
            exist_note = [n for n in self.notes if n.main_key == note_.main_key]
            
            if len(exist_note) == 0:
                self.notes.append(note_)
            else:
                exist_note[0].addSynonym(note_.synonyms, note_.cardinality)

        except Exception as e:
            print "User Class :: Error in copying a note  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise