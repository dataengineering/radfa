# -*- coding: utf-8 -*-
#!/usr/bin/python

import sys, traceback
reload(sys)
sys.setdefaultencoding('utf-8')

from RadFaObjects.resulted_data import ResultedData
from RadFaObjects.note import Note

"""
Represents the RadFa Analysis. Specifically, it aggregates and groups the filtered data by User.
Also,it determines the statistics of each FB.User based on these filtered data.
Fields:
        - my_nlp: type of <RadfaNLP> in order to call the RadFa's nlp functionality
        - filtered_data: type of <ResultedData> in order to store the resulting filtered data 
"""
class Analysis:
    def __init__(self, nlp_):
        self.my_nlp = nlp_
        self.filtered_data = ResultedData()


    """
    Aggregation procedure for data filtered <ResultedData> by phrase.
    Aggregate the filtered posts/comments and group them according to the posted FB.user.
        - Parameters:
                     post -> the original post where the message is enclosed
                     pid -> if not 0, it defines the id of the comment where the message is enclosed
                     phrase -> the phrase as str
        - Return Value: void
    """
    def aggregate_by_phrase(self, post, pid, phrase):
        try:
            if pid == 0:
                user = self.filtered_data.getUser(post['id'])
                
                # there is no post/comment made from the specific user id -> create a new user
                if user == None:
                    self.filtered_data.createUser(post['id'], post['name'])  #post['id'] is the user Id that made the post
                    self.filtered_data.addPost(post['id'], post)

                    self.filtered_data.addNote(post['id'], phrase)

                else:
                    user.addPost(post)
                    user.addNote(main_key=phrase)
                        
            else:
                #get the comment and then check if the uid of the comment exists in the filtered_data
                comment = [x for x in post['comments'] if x['id'] == pid][0]

                user = self.filtered_data.getUser(comment['from']['id'])

                if user == None:
                    self.filtered_data.createUser(comment['from']['id'], comment['from']['name'])
                    self.filtered_data.addComment(comment['from']['id'], comment, post)

                    self.filtered_data.addNote(comment['from']['id'], phrase)
                else:
                    user.addComment(comment, post)

                    user.addNote(main_key=phrase)
        except Exception as e:
            print "StatisticalProcedure in analysis_process :: Error to aggregate data filtered by phrase %s" % e
            traceback.print_exc(file=sys.stdout)
            raise


    """
    Aggregation procedure for data filtered <ResultedData> by keyword.
    Aggregate the filtered posts/comments and group them according to the posted FB.user.
        - Parameters:
                     post -> the original post where the message is enclosed
                     pid -> if not 0, it defines the id of the comment where the message is enclosed
                     token_list -> a list of tuples where its tuple contains a word and its POS
                     syns_set -> a list of synonyms
        - Return Value: void
    """
    def aggregate_by_keyword(self, post, pid, token_list, syns_set):
        try:
            #it is a post
            if pid == 0:
                user = self.filtered_data.getUser(post['id'])
                
                # there is no post/comment made from the specific user id -> create a new user
                if user == None:
                    self.filtered_data.createUser(post['id'], post['name'])  #post['id'] is the user Id that made the post
                    self.filtered_data.addPost(post['id'], post)

                    self.set_notes(post, 0, token_list, syns_set)

                else:
                    user.addPost(post)

                    self.set_notes(post, 0, token_list, syns_set)

            #it is a comment
            else:
                #get the comment and then check if the uid of the comment exists in the filtered_data
                comment = [x for x in post['comments'] if x['id'] == pid][0]

                user = self.filtered_data.getUser(comment['from']['id'])

                if user == None:
                    self.filtered_data.createUser(comment['from']['id'], comment['from']['name'])
                    self.filtered_data.addComment(comment['from']['id'], comment, post)

                    self.set_notes(post, pid, token_list, syns_set)
                else:
                    user.addComment(comment, post)
                    self.set_notes(post, pid, token_list, syns_set)

        except Exception as e:
            print "StatisticalProcedure in analysis_process :: Error to aggregate data filtered by keyword %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    """
    Determines the statistics and updates the notes for each user.
        - Parameters:
                     post -> the original post where the message is enclosed
                     pid -> if not 0, it defines the id of the comment where the message is enclosed
                     tokens -> a list of tuples where its tuple contains a word and its POS
                     syns -> a list of synonyms
        - Return Value: void
    """
    def set_notes(self, post, pid, tokens, syns):
        try:
            if pid == 0:
                user = self.filtered_data.getUser(post['id'])
            #it is a comment
            else:
                #get the comment and then check if the uid of the comment exists in the filtered_data
                comment = [x for x in post['comments'] if x['id'] == pid][0]
                user = self.filtered_data.getUser(comment['from']['id'])


            for syn_set in syns:
                
                if isinstance(syn_set[0], list):
                    temp_and = []   #it will contain the appeareances of each synonym and their cardinalities. if the AND condition is satisfied, the information will be kept.

                    # like a bitmap -> if all elements are True then AND condition is satisfied.
                    bitmap = [False] * len(syn_set)

                    # for each array of synonyms in AND condition
                    for i in range(0, len(syn_set)):
                        temp_note = Note(main_key_=syn_set[i][0])

                        for syn in syn_set[i]:
                            count = self.my_nlp.count_synonyms(syn, tokens)

                            if count != 0:
                                bitmap[i] = True

                                temp_note.synonyms.append(syn)
                                temp_note.cardinality = temp_note.cardinality + count

                                temp_and.append(temp_note)

                        if temp_note.cardinality == 0:
                            break

                    # all the elements of bitmap are True -> AND condition is satisfied for this message
                    if all(x == True for x in bitmap):
                        for temp_note in temp_and:
                            user.copyNote(temp_note)

                else:
                    for syn in syn_set:
                        count = self.my_nlp.count_synonyms(syn, tokens)
                    
                        if count != 0:
                            user.addNote(syn_set[0], syn, count)
        except Exception as e:
            print "StatisticalProcedure in analysis_process :: Error to apply the analysis in the filtered data %s" % e
            traceback.print_exc(file=sys.stdout)
            raise