# -*- coding: utf-8 -*-
#!/usr/bin/python

import nltk
import json
import sys, traceback
reload(sys)
sys.setdefaultencoding('utf-8')

from analysis_process import Analysis
from RadFaObjects.radfa_nlp import RadfaNLP

"""
The class contains the functionality for data filtering.
In other words, it is a pre-processing step to keep only the feed of posts that are interesting for the analysis.
Fields:
    - my_nlp: type of <RadfaNLP> in order to call the RadFa's nlp functionality
    - analysis: type of <Analysis> to aggregate the data and make the analysis
"""
class SearchFilter:
    def __init__(self):
        self.my_nlp = RadfaNLP()
        self.analysis = Analysis(self.my_nlp)


    def search_filtering(self, results, facet):
        fd = None
        try:
            syn_set = self.find_syn_keywords(facet)
            if not (syn_set == None):
                self.filter_keywords(results, syn_set)

            in_phrases = self.find_syn_phrases(facet)
            if not (in_phrases == None):
                self.filter_phrases_in(results, in_phrases)

            #convert the <ResultedData> object to JSON object
            fd = json.loads(json.dumps(self.analysis.filtered_data, encoding="utf-8", default=lambda o: o.__dict__))
        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error: %s" % e
            traceback.print_exc(file=sys.stdout)
        finally:
            return fd


    """
    Splits the keywords in a format that separates AND , OR keywords
        - Parameter: the keywords of the facet
        - Return Value: a list of keys. In the AND op. the keys are in a separate list into the main list. 
                        Ex.[ 'key1', ['key2a' , 'key2b'], 'key3'] 
                        where 'key2a' and 'key2b' are values for the AND operator, while the others are values for OR operator.
    """
    def get_keywords(self, keywords):
        try:
            if (keywords=="" or keywords == None):
                return None

            key_set = []
            or_separation = [keywords]

            #comma denotes OR op.
            if ',' in keywords:
                or_separation = [k.replace(" ","") for k in keywords.split(',')]

            for word in or_separation:
                #& denotes AND op.
                if '&' in word:
                    word = word.replace("(", "")
                    word = word.replace(")", "")
                    and_separation = word.split('&')

                    key_set.append(and_separation)
                else:
                    key_set.append(word)
        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error to find synonyms in phrases: %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
            return None
        else:
            return key_set



    """
    Split and get all the included phrases.
        - Parameter: the phrases of the facet
        - Return Value: a list of all phrases
    """
    def get_phrases(self, phrases_in):
        try:
            if (phrases_in=="" or phrases_in == None):
                return None

            phrasesIn = phrases_in.split(', ')
        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error to find synonyms in phrases: %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
            return None
        else:
            return phrasesIn


    """
    Find the synonyms of each word of every phrase
        - Parameter: the facet
        - Result Value: a list of lists of synonyms for each phrase.
                        Ex: 
                        [ 
                            [ ['word1_phrase1', 'synonym_word1_phrase1', ...], ... , ['wordK_phrase1', 'synonym_wordK_phrase1', ...] ],
                            ... ,
                            ... ,
                            [ ['word1_phraseM', 'synonym_word1_phraseM', ...], ... , ['wordP_phrase1', 'synonym_wordP_phraseM', ...] ]
                        ]
                        where M defines the number of phrases and K,P the K-th, P-th word of the respective phrase
    """
    def find_syn_phrases(self, facet):
        in_phrases = []
        try:
            phrasesIn = self.get_phrases(facet['phrasesIn'])

            if phrasesIn == None:
                return None

            else:
                self.my_nlp.setFields(phrasesIn)

            #for each phrase extract every word
            for ph in phrasesIn:
                phrase = ph.split()
                temp_ph = []    #it contains the lists of the synonyms for each word

                # for each word, define the synonyms and put them in a list (temp_keyset)
                for word in phrase:
                    try:
                        # get the synonyms for each word in the phrase
                        temp_keyset = self.my_nlp.get_synonyms(word, with_stem=False)
                    except Exception:
                        raise
                    
                    temp_ph.append(temp_keyset)

                #add the results of each phrase
                in_phrases.append(temp_ph)
        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error to find synonyms in phrases: %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
            return None
        else:
            return in_phrases


    """
    Find the stem of the synonyms for each keyword of the facet.
        - Parameter: the facet
        - Return Value: a list of a list of synonyms. In the AND op. the synonyms of each value are in a separate list 
                        into the main list. 
                        Ex: [ ['key1', 'synonym_key1'], 
                            ['key2', 'synonym_key2', ....], 
                            [['key3a', 'synonym_key3a', ...], ['key3b','synonym_key3b']] ]
                            where 'key3a' and 'key3b' are AND operators and the rest are OR op. 
    """
    def find_syn_keywords(self, facet):
        sets = []

        try:
            keywords = self.get_keywords(facet['keywords'])

            if keywords == None:
                return None
            else:
                self.my_nlp.setFields(keywords)
            
            for key in keywords:

                # key variable is a list -> AND operation
                if isinstance(key, list):
                    temp_and = []

                    try:
                        for k in key:
                            # get the stemmed synonyms for each AND op
                            temp_keyset = self.my_nlp.get_synonyms(k, with_stem=True)
                            temp_and.append(temp_keyset)

                        sets.append(temp_and)
                    except Exception:
                        raise
                        break
                else:
                    try:
                        # get the stemmed synonyms for each OR op
                        temp_keyset = self.my_nlp.get_synonyms(key, with_stem=True)
                    except Exception:
                        raise

                    sets.append(temp_keyset)

        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error: %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
            return None
        else:  
            return sets


    """
    Filter the messages/posts and keep the ones that contain the phrases.
    A message isn't filtered out if a phrase is included. 
    The words of the phrase (or their synonyms) should be in row in a message.
        - Parameters: 
                     results -> the searching results in json format
                     syn_phrases -> the list of synonyms of each word in each frame
        - Return Value: returns the filtered search data updated with the included phrases filtering procedure
    """
    def filter_phrases_in(self, results, syn_phrases):
        try:
            for post in results['posts']:
                if 'message' in post:
                    self.exist_phrase_in(post, 0, syn_phrases)

                if 'comments' in post:
                    #tokenize each comment and check if it contains a synonym.
                    for com in post['comments']:
                        if 'message' in com:
                            self.exist_phrase_in(post, com['id'], syn_phrases)
        except Exception as e:
            print "StatisticalProcedure in filtering_process:: Error in filtering phrases: %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
          

    """
    Checkes whether a message includes the specified phrase. 
    If it does, the filtered data are updated by calling the Analysis.aggregate_by_phrase() method.
        - Parameters:
                     post -> the original post where the message is enclosed
                     pid -> if not 0, it defines the id of the comment where the message is enclosed
                     syn_phrases -> the list of synonyms of all the phrases
        - Return Value: void
    """
    def exist_phrase_in(self, post, pid, syn_phrases):
        try:
            #it is a post
            if pid == 0:
                msg = post
            #it is a comment
            else:
                try:
                    msg = [com for com in post['comments'] if com['id'] == pid][0]
                except IndexError:
                    return

            #tokenize each message and check if it contains a synonym.
            token_list = nltk.word_tokenize(msg['message'])
            token_list = [token.lower() for token in token_list] 

            for phrase in syn_phrases:
                indices = []

                #get the indices of the first word + its synomyms into the token list
                for syn in phrase[0]:
                    indices = indices + [i for i, x in enumerate(token_list) if x == syn]

                for i in indices:
                    bitmap = [False] * (len(phrase)-1)

                    #checks if the phrase is formed in the message in row
                    for c in range(1,len(phrase)):
                        try:
                            if token_list[i+c] in phrase[c]:
                                bitmap[c-1] = True

                            if bitmap[c-1] == False:
                                break
                        except IndexError:
                            break

                    # if all the elements of bitmap are True, it denotes that the next words in the token list form the specified phrase
                    if all(x == True for x in bitmap):
                        str_phrase = ""
                        for item in phrase:
                            str_phrase = str_phrase + item[0]+" "

                        self.analysis.aggregate_by_phrase(post, pid, str_phrase)

        except Exception as e:
            print "Statistical Procedure in filtering_process :: Error in checking whether a phrase is included in a message or not  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise


    """
    Filter the posts and the comments and keep only those that contain the synonyms, 
    by taking into account the keywords of the faceted search.
        - Parameters:
                     results -> the searching results in json format
                     syn_set -> the list of synonyms of each keyword
        - Return Value: returns the filtered search data updated with the keywords filtering procedure
    """
    def filter_keywords(self, results, syn_set):
        try:
            for post in results['posts']:
                if 'message' in post:
                    self.exist_keyword(post, 0, syn_set)
                    
                if 'comments' in post:
                    #tokenize each comment and check if it contains a synonym.
                    for com in post['comments']:
                        if 'message' in com:
                            self.exist_keyword(post, com['id'], syn_set)
        except Exception as e:
            print "Statistical Procedure in filtering_process :: Error in filtering keywords %s" % e
            traceback.print_exc(file=sys.stdout)
            raise

    """
    Checkes whether a message includes the specified set of synonyms. 
    If it does, the filtered data are updated by calling the Analysis.aggregate_by_keyword() method.
        - Parameters:
                     post -> the original post where the message is enclosed
                     pid -> if not 0, it defines the id of the comment where the message is enclosed
                     syn_set -> the list of synonyms of all the keywords
        - Return Value: void
    """
    def exist_keyword(self, post, pid, syn_set):
        try:
            if pid == 0:
                msg = post
            else:
                try:
                    msg = [com for com in post['comments'] if com['id'] == pid][0]
                except IndexError:
                    return


            #tokenize each post and check if it contains a synonym.
            token_list = nltk.word_tokenize(msg['message'])
            token_list = [token.upper() for token in token_list]

            #get the POS tag of each word in the token list
            token_list = nltk.pos_tag(token_list)

            for syn_obj in syn_set:
                # AND condition
                if isinstance(syn_obj[0], list):
                    # like a bitmap -> if all elements are True then AND condition is satisfied.
                    bitmap = [False] * len(syn_obj)
                    
                    # for each array of synonyms in AND condition
                    for i in range(0, len(syn_obj)):
                        syn_existance = self.my_nlp.exist_synonym(syn_obj[i], token_list)

                        if syn_existance:
                            bitmap[i] = True
                        else:
                            break
                             
                    # all the elements of bitmap are True -> AND condition is satisfied for this message
                    if all(x == True for x in bitmap):
                        self.analysis.aggregate_by_keyword(post, pid, token_list, syn_set)
                        break
                else:
                    syn_existance = self.my_nlp.exist_synonym(syn_obj, token_list)
                    
                    if syn_existance:
                        self.analysis.aggregate_by_keyword(post, pid, token_list, syn_set)
                        break

        except Exception:
            print "Statistical Procedure in filtering_process :: Error in checking whether a set of keywords is included in a message or not  %s" % e
            traceback.print_exc(file=sys.stdout)
            raise
