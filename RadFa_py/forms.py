from django import forms
from django.contrib.admin.widgets import AdminDateWidget



class F_addfacet(forms.Form):
    name = forms.CharField(max_length=50,  widget=forms.TextInput(attrs={'placeholder':'Name', 'id':'nameF'}))
    key = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs={'placeholder':'Keyword', 'id':'keyF'}))
    tarea = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, "disabled":"disabled", 'id':'keywordsF'}))
    phrase_in = forms.CharField(max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder':'Phrases to Include', 'id':'phraseIn'}))
    tarea_phraseIn = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, "disabled":"disabled", 'id':'phraseInF'}))


class Hist(forms.Form):
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'placeholder': 'Name', 'id': 'nameF', "hidden":True }))

class MainSearch(forms.Form):
    CHOICES = (
        ('Default', ''),
    )

    groups = forms.ChoiceField(choices=CHOICES, required=True, widget=forms.Select(attrs={'id':'groups'}))
    facets = forms.ChoiceField(choices=CHOICES, required=True, widget=forms.Select(attrs={'id':'facets'}))

    fromTime = forms.DateField(widget=forms.SelectDateWidget(attrs={'required':False, 'placeholder': 'From', 'id': 'from'}))
    toTime = forms.DateField(widget=AdminDateWidget)

    def __init__(self, custom_choices=None, *args, **kwargs):
        super(MainSearch, self).__init__(*args, **kwargs)
        if custom_choices:
            self.fields['facets'].choices = custom_choices


class EditFacet(forms.Form):
    CHOICES = (
        (-1, ''),
    )

    efacets = forms.ChoiceField(choices=CHOICES, required=True, widget=forms.Select(attrs={'id': 'efacets'}))

    def __init__(self, custom_choices=None, *args, **kwargs):
        super(EditFacet, self).__init__(*args, **kwargs)
        if custom_choices:
            self.fields['efacets'].choices = custom_choices