from django.db import models

from RadFa_py.forms import MainSearch


class Facet(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30);
    keywords = models.TextField(null=True);
    phrasesIn = models.TextField(null=True);
    id_user = models.BigIntegerField();

    def publish(self):
        try:
            self.save()
        except Exception:
            raise Exception

    def __str__(self):
        return self.name + " " + self.keywords

    class Meta:
        db_table = 'RadFa_py_facet'


class FacetList(models.Model):
    id = models.AutoField(primary_key=True, unique=True, db_column='id') # Field name made lowercase.
    name = models.CharField(max_length=30, db_column='name')
    keywords = models.TextField(db_column='keywords')
    uid = models.BigIntegerField(db_column='id_user')

    class Meta:
        db_table = 'RadFa_py_facet'



class History(models.Model):
    id = models.AutoField(primary_key=True)
    fid = models.ForeignKey(Facet, on_delete=models.CASCADE)
    gid = models.BigIntegerField();
    gname = models.CharField(max_length=30);
    date = models.CharField(max_length=10);
    period = models.CharField(max_length=50);
    num_participants = models.IntegerField(default=0);
    id_user = models.BigIntegerField();
    details = models.TextField();

    def publish(self):
        self.save()

    def __str__(self):
        return self.fid + " " + self.gid

    class Meta:
        db_table = 'RadFa_py_history'

