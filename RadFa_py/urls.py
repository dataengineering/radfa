"""RadFa_py URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include,url
from django.contrib import admin
from RadFa_py import views
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin', include(admin.site.urls)),
    #url(r'', include('social_auth.urls')),
    url('', include('social_django.urls', namespace='social')),

    url(r'^login/', views.login, name='login'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^login_auth/', views.login_auth, name='login_auth'),
    url(r'^privacypolicyl/', views.privacypolicyl, name='privacypolicyl'),
    #url(r'^home/(\w+)/', views.home, name='home'),
    url(r'^home/', views.home, name='home'),
    url(r'^privacypolicy/', views.privacypolicy, name='privacypolicy'),
    url(r'^search_facet/', views.search_facet, name='search_facet'),
    url(r'^myfacets/', views.my_facets, name='myfacets'),
    url(r'^newfacet/', views.new_facet, name='newfacet'),
    url(r'^create_facet/', views.create_facet, name='create_facet'),
    url(r'^editfacet/', views.edit_facet, name='editfacet'),
    url(r'^editafacet/(?P<fid>[0-9]+)/$', views.edit_afacet, name='editafacet'),
    url(r'^editafacet/', views.edit_a_facet, name='editafacet'),
    url(r'^update_facet/', views.update_facet, name='update_facet'),
    url(r'^delete_facet/', views.delete_facet, name='delete_facet'),
    url(r'^editf/', views.editf, name='editft'),
    url(r'^history/', views.history, name='history'),
    url(r'^save_history/', views.save_history, name='save_history'),
    url(r'^delete_history/', views.delete_history, name='delete_history'),
    url(r'^myhistory/(?P<hid>[0-9]+)/$', views.myhistory, name='myhistory'),
]