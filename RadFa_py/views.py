from django.http import HttpResponse
from django.shortcuts import render, redirect
from RadFa_py.forms import F_addfacet, MainSearch, EditFacet, Hist
from django.template import loader,Context
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import ensure_csrf_cookie
from RadFa_py.StatisticalProcedure.filtering_process import SearchFilter
from RadFa_py.models import Facet, History

import json, time


@ensure_csrf_cookie
def login(request):
	return render(request, "../templates/html/login.html") 

def login_auth(request):
    if request.method == 'POST':
        uid = request.POST.get('uid')
        request.session['uid'] = uid
        response_data = {}

        response_data['result'] = 'Successful login!'
        response_data['uid'] = uid

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )


def logout(request):
    if request.method == 'POST':
        request.session['uid'] = -1

        return HttpResponse(
            json.dumps({"Result: ": "Successful logout"}),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )


def home(request):
    uid=request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")
    else:
        try:
            facetsL = Facet.objects.filter(id_user=uid)

            choices = []
            for fac in facetsL:
                choices.append((fac.id, fac.name))


            if request.method == "POST":
                searchForm = MainSearch(request.POST)

            else:
                searchForm = MainSearch(choices)


            group = {}
            group['name'] = "-";
            group['id'] = -1;

            facet = {};
            facet['name'] = "-";
            facet['id'] = -1;

            return render(request, "../templates/html/home.html", {"posts": [], 'facets':facetsL, "searchF":searchForm, "group":group, "facet":facet, "emptyR": 'False'})

        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please try again later."}),
                content_type="application/json"
            )
    

def privacypolicy(request):
    uid=request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")
    else:
        try:
            return render(request, "../templates/html/privacypolicy.html") 

        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please try again later."}),
                content_type="application/json"
            )

def privacypolicyl(request):
    return render(request, "../templates/html/privacypolicyl.html") 

def search_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        posts = json.loads(request.POST.get('posts'))
        facet = request.POST.get('facet')
        group = json.loads(request.POST.get('group'))
        fromD = request.POST.get('from')
        toD = request.POST.get('to')

        facetsL = Facet.objects.filter(id_user=uid)

        choices = []

        current_f = {}
        for fac in facetsL:
            choices.append((fac.id, fac.name))
            if str(fac.id) == str(facet):
                current_f["name"] = fac.name
                current_f["id"] = fac.id
                current_f["keywords"] = fac.keywords
                current_f["phrasesIn"] = fac.phrasesIn


        if request.is_ajax():
            
            filter_procedure = SearchFilter()
            fp = filter_procedure.search_filtering(posts, current_f)
           
            if fp == None:
                return HttpResponse(
                    json.dumps({"Error": 
                                "Please check the search criteria or try again later ..."}),
                    content_type="application/json"
                )

            searchForm = MainSearch(choices)

            c = Context({'posts': fp['users'], 'facets':facetsL, "searchF":searchForm, "group":group, "facet":current_f , "from": fromD, "to":toD })

            return render(request, '../templates/html/home.html', c)
        else:
            return HttpResponse(
                json.dumps({"Error": "Security Error !!!"}),
                content_type="application/json"
            )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )


def my_facets(request):
    uid = request.session['uid']

    if uid is None:
        return render(request, "../templates/html/login.html")

    try:
        facetsL = Facet.objects.filter(id_user=uid)
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

    return render(request, "../templates/html/myfacets.html",{'facets':facetsL })


def new_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    try:
        facetsL = Facet.objects.filter(id_user=uid)

        if request.method == "POST":
            addFacetForm = F_addfacet(request.POST)

        else:
            addFacetForm = F_addfacet()
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

    return render(request, "../templates/html/newfacet.html", {'form': addFacetForm, 'facets':facetsL })


def edit_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    try:
        facetsL = Facet.objects.filter(id_user=uid)

        choices = []
        for fac in facetsL:
            choices.append((int(fac.id), fac.name))

        editForm = EditFacet(choices)
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

    return render(request, "../templates/html/editfacet.html", {'facets':facetsL, "editF":editForm })


def editf(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == "POST":
        fid = int(request.POST.get('fid'))

        return redirect(reverse('editafacet',  kwargs={'fid': fid} ))

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

"""
When the RadFa user chooses a facet from the button (ex in myfacets page).
"""
def edit_afacet(request, fid):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    response_data = {}
    response_data['id'] = fid

    facetsL = Facet.objects.filter(id_user=uid)

    if not facetsL:
    	editForm = EditFacet(choices)
    	return render(request, "../templates/html/editfacet.html", {'facets':facetsL, "editF":editForm  })

    else:
        try:
    	    choices = []
    	    for fac in facetsL:
    	        choices.append((fac.id, fac.name))
    	        if str(fac.id) == str(fid):
    	            response_data['name'] = fac.name
    	            response_data['keywords'] = fac.keywords
                    response_data['phrasesIn'] = fac.phrasesIn
    	    

    	    if request.method == "POST":
    	        updateFacetForm = F_addfacet(request.POST,initial={'name': response_data['name'], 'tarea': response_data['keywords'], 'tarea_phraseIn': response_data['phrasesIn'] })

    	    else:
    	        updateFacetForm = F_addfacet(initial={'name': response_data['name'], 'tarea': response_data['keywords'], 'tarea_phraseIn': response_data['phrasesIn'] })

    	    response_data['keys'] = response_data['keywords'].split(', ')

            response_data['phrases_in'] = response_data['phrasesIn'].split(', ')

            return render(request, "../templates/html/editafacet.html", {'facets':facetsL, "results":response_data, "update":updateFacetForm })

        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please check that you access an existed facet or try again later."}),
                content_type="application/json"
            )
	    
"""
When the RadFa user chooses a facet from the drop down list.
"""
def edit_a_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.GET.get('efacets'):
        fid = request.GET['efacets']
    else:
        fid = -1

    response_data = {}
    response_data['id'] = fid
    response_data['name'] = ''
    response_data['keywords']  = ''

    facetsL = Facet.objects.filter(id_user=uid)

    if not facetsL:
    	editForm = EditFacet(choices)
    	return render(request, "../templates/html/editfacet.html", {'facets':facetsL, "editF":editForm  })

    try:
        choices = []
        for fac in facetsL:
            choices.append((fac.id, fac.name))
            if str(fac.id) == str(fid):
                response_data['name'] = fac.name
                response_data['keywords'] = fac.keywords
                response_data['phrasesIn'] = fac.phrasesIn


        if request.method == "POST":
            updateFacetForm = F_addfacet(request.POST,initial={'name': response_data['name'], 'tarea': response_data['keywords'], 'tarea_phraseIn': response_data['phrasesIn'] })

        else:
            updateFacetForm = F_addfacet(initial={'name': response_data['name'], 'tarea': response_data['keywords'], 'tarea_phraseIn': response_data['phrasesIn'] })

        response_data['keys'] = response_data['keywords'].split(', ')
        response_data['phrases_in'] = response_data['phrasesIn'].split(', ')

        return render(request, "../templates/html/editafacet.html", {'facets':facetsL, "results":response_data, "update":updateFacetForm })
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please check that you access an existed facet or try again later."}),
            content_type="application/json"
        )



def myhistory(request, hid):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    try:
        response_data = {}
        response_data['id'] = hid

        myhist = History.objects.get(id=int(hid))

        response_data['gname'] = myhist.gname
        response_data['facet'] = myhist.fid
        response_data['date'] = myhist.date
        response_data['period'] = myhist.period
        response_data['num_p'] = myhist.num_participants
        response_data['det'] = json.loads(myhist.details)

        jsonObj = json.loads(response_data['det'])
        jsonRes = []
        for obj in jsonObj:
            rr = {}
            rr['name'] = obj['name']

            stats=[]
            for st in obj['stats']:
                stats.append(st)

            rr['stats'] = stats

            posts = []
            for p in obj['posts']:
                pp = {}
                pp['created_time'] = p['createdTime']
                pp['message'] = p['message']
                pp['likes'] = p['likes']
                pp['comments'] = p['comments']

                posts.append(pp)

            rr['posts'] = posts

            jsonRes.append(rr)

            return render(request, "../templates/html/myhistory.html", {"results":response_data , "posts": jsonRes})
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please check that you access an existed history or try again later."}),
            content_type="application/json"
        )

    

def history(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    try:
        facetsL = Facet.objects.filter(id_user=uid)

        myhistory = History.objects.filter(id_user=uid)

        return render(request, "../templates/html/history.html", {'facets':facetsL , 'myhistory': myhistory })
    except Exception:
        return HttpResponse(
            json.dumps({"Error": "Please check that you access an existed history or try again later."}),
            content_type="application/json"
        )


################################### FROM AJAX REQUESTS ###################################

def create_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        try:
            name = request.POST.get('name')
            keywords = request.POST.get('keywords')
            phrasesIn = request.POST.get('phrases_in')
            response_data = {}

            facet = Facet(name=name, keywords=keywords, phrasesIn=phrasesIn, id_user=uid)
            facet.publish()

            response_data['result'] = 'Create post successful!'
            response_data['name'] = name
            response_data['keywords'] = keywords

        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please check that you have completed the attributes of the facet in a proper way or try again later."}),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )


def update_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        try:
            name = request.POST.get('name')
            keywords = request.POST.get('keywords')
            fid = request.POST.get('fid')
            phrasesIn = request.POST.get('phrases_in')
            response_data = {}

            facet_edit = Facet.objects.get(id=int(fid))  # object to update

            if not facet_edit:
                return HttpResponse(
                json.dumps({"Error": "Please check that you access the right facet or try again later."}),
                content_type="application/json"
            )

            facet_edit.name = name
            facet_edit.keywords = keywords
            facet_edit.phrasesIn = phrasesIn
            facet_edit.publish()  # save object

            response_data['result'] = 'Create post successful!'
            response_data['name'] = name
            response_data['keywords'] = keywords
            response_data['fid'] = fid
        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please check that you have completed the attributes of the facet in a proper way or try again later."}),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

def delete_facet(request):
    uid = request.session['uid']

    if not uid or uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        fid = request.POST.get('fid')

        instance = Facet.objects.get(id=fid)

        if not instance:
        	return HttpResponse(
            json.dumps({"Error": "You are tying to delete a facet that does not exist.", "fid":fid}),
            content_type="application/json"
        )

        instance.delete()

        return HttpResponse(
            json.dumps({"delete": "ok", "fid":fid}),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )


def save_history(request):
    uid = request.session['uid']

    if uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        try:
            group = request.POST.get('group')
            groupName = request.POST.get('groupName')
            fid = request.POST.get('facet')
            period = request.POST.get('period')
            details = json.dumps(request.POST.get('details'))
            num = request.POST.get('num')
            response_data = {}

            cur_date = time.strftime("%d/%m/%Y")

            facet = Facet.objects.get(id=fid)
            hist = History(fid=facet, gid=group, gname=groupName, id_user=uid, date=cur_date, period=period, details=details, num_participants=num) #num_participants=len(details) likes=nums[0], comments=nums[1], posts=nums[2]
            hist.publish()

            response_data['result'] = 'Create post successful!'
            response_data['fid'] = fid
            response_data['gid'] = group
            response_data['uid'] = uid
            response_data['period'] = period
            response_data['cur_date'] = cur_date
            response_data['details'] = details

        except Exception:
            return HttpResponse(
                json.dumps({"Error": "Please check that you store the right results or try again later."}),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )

def delete_history(request):
    uid = request.session['uid']

    if uid is None:
        return render(request, "../templates/html/login.html")

    if request.method == 'POST':
        hid = request.POST.get('hid')

        instance = History.objects.get(id=hid)

        if not instance:
        	return HttpResponse(
            json.dumps({"Error": "You are tying to delete a history instance that does not exist", "hid":hid}),
            content_type="application/json"
        )

        instance.delete()

        return HttpResponse(
            json.dumps({"delete": "ok", "hid":hid}),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"Error": "Please try again later."}),
            content_type="application/json"
        )