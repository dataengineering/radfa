/**
 * Created by katerina on 13/1/2017.
 */

 $(document).ready(function() {
    $('#addK').click(function(event) {
        var key = $("#keyF").val();
        var tareaFacet = $("#keywordsF").val();

        if(tareaFacet) {
            tareaFacet = tareaFacet + ", "+ key;
            $("#keywordsF").text(tareaFacet);
        }
        else {
            $("#keywordsF").text(key);
        }

        $("#keyF").val("");
    });

    $('input').keypress(function (event) {
        var regex = new RegExp("^[a-zA-Z0-9\_\-]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

        //this character is not allowed
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }

    });

});