/**
 * Created by katerina on 17/1/2017.
 */


$(document).ready(function() {

    $('#addK').click(function(event) {
        var key = $("#keyF").val();
        var tareaFacet = $("#keywordsF").val();

        if (key.indexOf('&') > -1) {
            key = '('+key+')'
        }

        if(tareaFacet) {
            tareaFacet = tareaFacet + ", "+ key;
            $("#keywordsF").text(tareaFacet);
        }
        else {
            $("#keywordsF").text(key);
        }

        $("#keyF").val("");
    });

    $('#addPin').click(function(event) {
        var key = $("#phraseIn").val();
        var tareaFacet = $("#phraseInF").val();

        if(tareaFacet) {
            tareaFacet = tareaFacet + ", "+ key;
            $("#phraseInF").text(tareaFacet);
        }
        else {
            $("#phraseInF").text(key);
        }

        $("#phraseIn").val("");
    });



    $('#cancelF').click(function (event) {
        event.preventDefault();
        $("#keywordsF").text("");
        $("#phraseInF").text("");
        document.getElementById("addFacet").reset();
    });

    $('input').keypress(function (event) {
        console.log('pressed!!!');
        var regex = new RegExp("^[a-zA-Zά-ωΑ-ώ0-9\\s\_\&\b\-]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

        if (!regex.test(key)) {
            event.preventDefault();
            console.log("This character is not allowed.");
            return false;
        }


    });

    $('#addFacet').on('submit', function(event){

        if($('#nameF').val()=="" || $('#nameF').val()==null) {
            alert("You should fill a name in your facet.");
            return false;
        }
        if(($('#keywordsF').val()=="" || $('#keywordsF').val()==null) && ($('#phraseInF').val()=="" || $('#phraseInF').val()==null)) {
            alert("You should fill at least one keyword or phrase in your facet.");
            return false;
        }
        event.preventDefault();
        create_facet();
    });


    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

        // AJAX for posting
    function create_facet() {

        /* Getting the token and setting it on the AJAX request */
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $.ajax({
            url : "/create_facet/", // the endpoint
            type : "POST", // http method
            data : { name: $('#nameF').val(), keywords : $('#keywordsF').val(), phrases_in : $('#phraseInF').val() }, // data sent with the post request

                // handle a successful response
            success : function(json) {
                $('#nameF').val(''); // remove the value from the input
                $('#keywordsF').val(''); // remove the value from the input
                $('#keyF').val(''); // remove the value from the input
                $('#phraseInF').val('');
                
                console.log("success"); // another sanity check
                if(json.hasOwnProperty('Error')){
                    var error = "<div class='posts_res'>An unexpected error occuded: "+ json['Error'] +"</div>"
                    $('div#main_body').html(error)
                    console.log("Error: "+json['Error']);
                }
                else {
                    window.location = "/myfacets/"
                }

            },

                // handle a non-successful response
            error : function(xhr,errmsg,err) {
                $('#err_results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+ errmsg +
                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    };




 });
