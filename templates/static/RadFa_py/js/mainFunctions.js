/**
 * Created by katerina on 23/1/2017.
 * Main functonality for login,logout and fetching basic information
 */
var uid = 0;

$(document).ready(function() {

  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      connectAPI();
    } else if (response.status === 'not_authorized') {
        window.location = "/login/";
    } else {
        deleteAllCookies();
        window.location = "/login/";
    }
  }


  function checkLoginState() {

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {

      FB.init({
        appId      : '730308040455518',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.8'
      });

      FB.getLoginStatus(function(response) {
          console.log("Ready to check the user's status");
          statusChangeCallback(response);

      });

      FB.Event.subscribe('auth.login', function () {
          window.location = "/home/";
      });

      FB.Event.subscribe('auth.logout', function () {
          console.log("About to logout ... ")
          deleteAllCookies();
          window.location = "/login/";
      });

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function connectAPI() {
    console.log('Welcome!  Fetching your information.... !');

    FB.api('/me', function(response) {
      document.getElementById('status').innerHTML = 'Welcome ' + response.name + '!';
      uid = response.id;

      getMyGroups(response.id);
    });
  }


  function getAccessToken() {
      var access_token =   FB.getAuthResponse()['accessToken'];
      return access_token;
  }

  function deleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/"; //expires=Thu, 01 Jan 1970 00:00:00 GMT
      }
  }

  /* get the groups that belong to the logged in user */
  function getMyGroups() {
      var groupCall= '/' + uid + '/groups';

      FB.api(groupCall, function (response) {

            if (response && !response.error) {

                var selectGroup = document.getElementById('group');

                if(selectGroup != null) {
                    //for each group
                    for (var i in response.data) {
                        var opt = document.createElement('option');
                        opt.value = response.data[i].id;
                        opt.innerHTML = response.data[i].name;
                        selectGroup.appendChild(opt);

                    }
                }

            }
            else {
                  document.getElementById('groupRes').innerHTML = 'Error loading groups...';
            }
        }
      );
  }

  function getGroupName(gid) {
      var groupCall= '/' + uid + '/groups';

      FB.api(groupCall, function (response) {

            if (response && !response.error) {
                for (var i in response.data) {
                    if (response.data[i].id == gid)
                        return response.data[i].name;
                }

            }
            else {
                  return 'Error loading group...';
            }
        }
      );
  }
});

function getAccessToken() {
      var access_token =   FB.getAuthResponse()['accessToken'];
      return access_token;
  }

function getId() {
    return uid;
}



function covertToTime(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();

    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;

    return time;
}