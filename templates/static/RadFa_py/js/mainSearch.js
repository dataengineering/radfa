/**
 * Created by katerina zamani on 25/1/2017.
 */

var app = {
  id        : '730308040455518',
  secret    : '9074cbde56eabd579bb8c8dfdc0d0721'
};
var fb = {
    end_point: "https://graph.facebook.com",

    api: function(uri, type, data, success_callback, error_callback){

        if( data.batch!= undefined  ){ // if making a batch call
            data.batch = JSON.stringify(data.batch); // batch parameter must be a JSON array
        }

        $.ajax({
            url: this.end_point+uri,
            type: type,
            data: data,
            dataType: "json",
            crossDomain: true,

            success: function(response){
                try{
                    success_callback(response);
                } catch(e){ };
            },
            error: function(jqXHR, textStatus, errorThrown){
                try{
                    error_callback(jqXHR, textStatus, errorThrown);
                } catch(e){};
            }
        });
    }
};

var $loading = $('#load_image').show();

$(document).ready(function() {
    

    $('#searchF').on('submit', function (event) {

        event.preventDefault();

        search();
    });

    $('#btn_reset').on('click', function (event) {

        event.preventDefault();
        document.getElementById("searchF").reset();
    });

    /*Convert a day to Timestamp in GMT*/
    function convertToTimestamp(time) {
        var dayParts = time.split('-');
        var day = new Date(dayParts[2], parseInt(dayParts[1], 10) - 1, dayParts[0], 0, 0);

        var timestamp = Math.floor(day.getTime() / 1000);

        return timestamp;
    }

    function covertToTime(timestamp) {
        var c_date = timestamp.split("T");
        var p_date = c_date[0];
        var p_time = c_date[1].substr(0,5)

        var time = p_date +"   "+p_time;

        return time;
    }

    function search() {
        var tot_com = 0;
        var tot_likes = 0;
        var posts = [];
        var resultedP = {};
        var group = {};
        var likes = "F";
        var comments = "F";
        var fromD= $("#Fdatepicker").val();
        var toD = $("#Tdatepicker").val();

        group['name'] = $('#group').find('option:selected').text();
        group['id'] = $('#group').val();


        if(document.getElementById('likes').checked) {
            likes = "T";
        }
        if(document.getElementById('comments').checked) {
            comments = "T";
        }

        var timeStr = "?"
        if(fromD != "") {
            timeStr = timeStr + "since="+convertToTimestamp(fromD)+"&";
        }

        if(toD != "") {
            timeStr = timeStr + "until="+convertToTimestamp(toD)+"&";
        }

        var args = {
            access_token: FB.getAuthResponse()['accessToken'],//app.id + "|" + app.secret,
            batch: []
        };

        args.batch.push({
            method: "GET",
            name: "get-users",
            relative_url: "/"+group['id']+"/feed"+timeStr, // "/"+app.id+"accounts/test-users?limit=500"
            omit_response_on_success: false
        });

        args.batch.push({
            method: "GET",
            relative_url: "?ids={result=get-users:$.data.*.id}&fields=from,comments,likes"
        });


        console.log("ready to request");
        // make batch API call to get more information about this user. notice how we are submitting a POST to /
        fb.api("", "POST", args, function (batch_response) { // success_callback

            var body = JSON.parse(batch_response[0].body)  // we need to get post's info

            if(body.data.length != 0) {

                $.each(body.data, function (i, post) {
                    posts[i] = post;
                    posts[i]['id_p'] = post.id;
                    posts[i]['created_time'] = covertToTime(post.updated_time);

                });

                body = JSON.parse(batch_response[1].body);
                var k = 0;

                $.each(body, function (id, user) {

                    $.each(user.from, function (key, value) { // get each peace
                        posts[k][key] = value;
                    });

                    if (comments == "T") {
                        if (user.hasOwnProperty("comments")) {
                            posts[k]['comments'] = [];
                            $.each(user.comments.data, function (i, comment) { // get each peace
                                posts[k]['comments'][i] = comment;
                                posts[k]['comments'][i]['created_time'] = covertToTime(posts[k]['comments'][i]['created_time']);
                                tot_com++;

                            });
                        }
                    }

                    if (likes == "T") {
                        if (user.hasOwnProperty("likes")) {
                            posts[k]['likes'] = [];
                            $.each(user.likes.data, function (i, like) { // get each peace
                                posts[k]['likes'][i] = like;
                                tot_likes++;
                            });
                        }
                    }

                    k++;

                    //user_info.push(posts[id]); // push to user_info array
                });

                resultedP['tot_comments'] = tot_com;
                resultedP['tot_likes'] = tot_likes;
                resultedP['posts'] = posts;
            }

            var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        $('#load_image').show();
                    }
                }
            });


             $.ajax({
                url : "/search_facet/", // the endpoint
                type : "POST", // http method
                data : { posts: JSON.stringify(resultedP), facet : $('#facets').val() , group : JSON.stringify(group), from: fromD, to: toD, emptyR: 'True' /*$('#group').find('option:selected').text()*/}, // data sent with the post request

                complete: function(){
                    $('#load_image').hide();
                },
                // handle a successful response
                success : function(data) {
                    console.log("success!!"); // another sanity check
                    if(data.hasOwnProperty('Error')){
                        var error = "<div id='res'>An unexpected error occuded: "+ data['Error'] +"</div>"
                        $('div#main_body').html(error)
                        console.log("Error: "+data['Error']);
                    }
                    else {
                        dataHtml = $($.parseHTML(data)).find('#res')
                        $('div#main_body').html(dataHtml);
                    }

                },
                error : function(xhr,errmsg,err) {
                    var error = "<div id='res'>Oops! We have encountered an error: "+ errmsg +"</div>"
                    $('div#main_body').html(error) // add the error to the dom
                    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                }
            });


        }, function (jqXHR, textStatus, errorThrown) { // error_callback
            var response_text = JSON.parse(jqXHR.responseText);

            $.each(response_text.error, function (k, v) {
                console.log(k + " : " + v);
            });
        }); // end of fb.api call

    }
    

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
});

