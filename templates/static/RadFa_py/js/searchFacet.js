/**
 * Created by katerina on 24/1/2017.
 */


$(document).ready(function() {

    $('#searchF').on('submit', function (event) {

        event.preventDefault();
        console.log("Main Search : form submitted!")  // sanity check
        search_facet();
    });

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function getPosts(gid, pst) {
        var groupCall= '/' + gid + '/feed';
        var posts = [];

        console.log("groupid = "+gid)
         /*   FB.api(groupCall, function (response) {

                    if (response && !response.error) {
                        console.log("DATA " + response.data)

                        for (var i in response.data) {

                            getAuthor(response.data.id, function (name) {
                                console.log(name)
                                posts.push(name)
                            });


                            var singlePost = {};
                            singlePost['id'] = response.data[i].id;
                            singlePost['message'] = response.data[i].message;

                            getAuthor(singlePost['id'], function (name) {
                                singlePost['from'] = name;
                                console.log('FRom '+singlePost['from']);

                            });

                            console.log("F = " + singlePost['from']);

                            posts.push(singlePost);

                        }

                    }

                    pst(posts);
            });*/
        getPostSpecs(gid, function(pp) {
            /*for(var k in pp)
            {
                getAuthor(pp[k].id, function (name) {
                    pp[k]['from'] = name;
                    console.log('FRom ' + pp[k]['from']);

                });

                console.log("F = " + pp[k]['from']);
            }*/

            pst(pp);
        });
        //pst(posts);
    }

    function getPostSpecs(gid, pp) {
        var groupCall= '/' + gid + '/feed';

        FB.api(groupCall, function (response) {
                posts = [];
                    if (response && !response.error) {
                        /* handle the result */
                        console.log("DATA " + response.data)

                        for (var i in response.data) {

                            getAuthor(response.data.id, function (name) {
                                console.log(name)
                                posts.push(name)
                            });


                            var singlePost = {};
                            singlePost['id'] = response.data[i].id;
                            singlePost['message'] = response.data[i].message;

                            /*getAuthor(singlePost['id'], function (name) {
                                singlePost['from'] = name;
                                console.log('FRom '+singlePost['from']);

                            });

                            console.log("F = " + singlePost['from']);*/

                            posts.push(singlePost);

                        }

                    }
                pp(posts)


            });
    }

    function getAuthor(pid, getName) {
        var url = '/' + pid + '?fields=from'
        FB.api(url, function (responseFrom) {

            if (responseFrom && !responseFrom.error) {
                getName(responseFrom.from.name);
            }
        });
    }


    function getDetailPosts(gid, pst) {
        var groupCall= '/' + gid;
        var posts = []
        /*
        FB.api("/", method="POST", {
            batch: [
                {method: 'GET', name: 'gPosts', relative_url: gid+'/feed'},
                {method: "GET", relative_url: "{result=gPosts:$.data.*.id}?fields=from"}
            ]
        },
        function (response) {
            if (response && !response.error) {
                console.log("RESPONSE "+ response.data);
                posts.push(response.data)
            }
            else {
                console.log("ERROR "+response.error.message);
            }


            pst(posts);
        });*/



       var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        })

        var args = {
            access_token: FB.getAuthResponse()['accessToken'],
            batch: []
        };

        args.batch.push({
            method                      : "GET",
            name                        : "gposts",
            relative_url                : "/"+gid+"/feed", // by default the limit is 50, so get all 500
            omit_response_on_success    : false
        });
        args.batch.push({
            method        : "GET",
            relative_url  : "?ids={result=gposts:$.data.*.id}?fields=from"
        });

       $.ajax({
            url: 'https://graph.facebook.com/',
            type: 'POST',
            data : args,

           success: function(response){
            console.log("EDWWW "+response);

           }

        });


    }


    // AJAX for posting
    function search_facet() {
        console.log("main search is working! "); // sanity check
        getDetailPosts($('#group').val(), function (posts) {



            //console.log("posts "+posts[0].id+" l = "+posts.length);
            /* Getting the token and setting it on the AJAX request */
            var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

            $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
            });

             $.ajax({
                url : "/search_facet/", // the endpoint
                type : "POST", // http method
                data : { group: posts, facet : $('#facets').val() , uid:FB.getAuthResponse()['userID']}, // data sent with the post request

                // handle a successful response
                success : function(json) {
                    console.log(json); // log the returned json to the console
                    console.log("success"); // another sanity check
            },
            error : function(xhr,errmsg,err) {
                    $('#err_results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+ errmsg +
                        " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                }
            });

            /*
            $.ajax({
                url : "/search_facet/", // the endpoint
                type : "POST", // http method
                async: false,
                data : { group: posts, facet : $('#facets').val() , uid:FB.getAuthResponse()['userID']}, // data sent with the post request

                // handle a successful response
                success : function(json) {
                    console.log(json); // log the returned json to the console
                    console.log("success"); // another sanity check

                    //window.location = "/home/"

                    for(var k in posts) {
                        $.ajax({
                            type: 'GET',
                            url: 'https://graph.facebook.com/'+posts[k].id+'?fields=from',
                            async: false,
                            data: {
                                access_token: FB.getAuthResponse()['accessToken'],//received via response.authResponse.accessToken after login
                            },
                            success: function (response) {
                                if (response && !response.error) {
                                    posts[k].from = response.from.name;
                                    console.log("FROM = " + posts[k]['from']);
                                }
                            }
                        });
                    }

                },

                // handle a non-successful response
                error : function(xhr,errmsg,err) {
                    $('#err_results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+ errmsg +
                        " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                }
            });
            */

            //////////////////////////////////////////

            /*
            for(var k in posts) {
                $.ajax({
                    type: 'GET',
                    url: 'https://graph.facebook.com/'+posts[k].id+'?fields=from',
                    async: false,
                    data: {
                        access_token: FB.getAuthResponse()['accessToken'],//received via response.authResponse.accessToken after login
                    },
                    success: function (response) {
                        if (response && !response.error) {
                            posts[k].from = response.from.name;
                            console.log("FROM = " + posts[k]['from']);
                        }



                        $.ajax({
                            url : "/search_facet/", // the endpoint
                            type : "POST", // http method
                            async: false,
                            data : { group: posts, facet : $('#facets').val() , uid:FB.getAuthResponse()['userID']}, // data sent with the post request

                            // handle a successful response
                            success : function(json) {
                                console.log(json); // log the returned json to the console
                                console.log("success"); // another sanity check



                            },

                            // handle a non-successful response
                            error : function(xhr,errmsg,err) {
                                $('#err_results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+ errmsg +
                                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                                 console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                            }
                        });
                    },
                    error : function(xhr,errmsg,err) {
                        $('#err_results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+ errmsg +
                        " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                    }
            });
         }

         */
            /////////////////////////////////


        });
    };

 });
